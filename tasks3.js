// class FootballPlayer {
//     constructor(speed, name, surname, height, age) {
//         this.speed = speed;
//         this.name = name;
//         this.surname = surname;
//         this.height = height;
//         this.age = age;
//     }

//     getFullName = function(){
//         return this.name + ' ' + this.surname
//     }
// }

// const palyer1 = new FootballPlayer(15, 'Jonas', 'Petrauskas', 180, 25);
// const palyer2 = new FootballPlayer(17, 'Vardenis', 'Pavardenis', 186, 23);
// const palyer3 = new FootballPlayer(14, 'Deividas', 'Raudys', 172, 29);

// const players = [palyer1, palyer2, palyer3];
// // Sort by player speed, fastest will be first in the array
// players.sort((a, b) => b.speed - a.speed)

// players.forEach(player => {
//     const newDiv = document.createElement('div')
//     newDiv.innerText = `${player.getFullName()}`
//     document.body.appendChild(newDiv)
// })


// const newDiv = document.createElement('div')
// newDiv.innerText = `Greičiausias žaidėjas yra: ${players[0].getFullName()}`
// newDiv.style.paddingTop = '10px'
// document.body.appendChild(newDiv)



// ---------------------2 dalis ----------------------

class Car {
    constructor(brand, speed, distance){
        this.brand = brand
        this.speed = speed
        this.distance = distance
    }

    distancePerHour = function(time, distance){
        const speed = distance / time
        this.speed = speed
        this.distance = distance
    }
}

// Method test
const car0 = new Car('BMW', 100, 200);
console.log(car0);
car0.distancePerHour(2, 300);
console.log(car0);

// Form
const form = document.querySelector('form');

form.addEventListener('submit', (e)=> {
    e.preventDefault();

    const formData = new FormData(form);
    
    const car1Input = formData.getAll('car1');
    const car2Input = formData.getAll('car2');
    const car3Input = formData.getAll('car3');
    const car4Input = formData.getAll('car4');
    const car5Input = formData.getAll('car5');

    const car1 = new Car(car1Input[0], car1Input[1], car1Input[2])
    const car2 = new Car(car2Input[0], car2Input[1], car2Input[2])
    const car3 = new Car(car3Input[0], car3Input[1], car3Input[2])
    const car4 = new Car(car4Input[0], car4Input[1], car4Input[2])
    const car5 = new Car(car5Input[0], car5Input[1], car5Input[2])
    
    const cars = [car1, car2, car3, car4, car5]

    for(let i = 0; i < cars.length; i++) {
        car = cars[i]
        const newDiv1 = document.createElement('div')
        newDiv1.innerText = `Mašina: ${car.brand}, Greitis: ${car.speed} km/h, Kelias: ${car.distance} km`
        document.body.appendChild(newDiv1)
    }
    
    // Fastest car
    cars.sort((a, b) => b.speed - a.speed)
    const newDiv1 = document.createElement('div')
    newDiv1.innerText = `Greičiausia mašina: ${cars[0].brand}, Greitis: ${cars[0].speed} km/h, Kelias: ${cars[0].distance} km`
    newDiv1.style.paddingTop = '20px'
    document.body.appendChild(newDiv1)

})
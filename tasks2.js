// function iLikeToProgram(){
//     alert('Mėgstu programuoti.Yey')
// }


// const btn = document.createElement('button');
// btn.innerText = 'Call I like to Program function';
// btn.addEventListener('click', ()=> {
//     iLikeToProgram()
// });
// document.body.appendChild(btn)


// const fruit = 'Bananas';
// const price = 1

// function fiveCost(fruit, price) {
//     const fivePrice = price * 5
//     console.log('Five ' + fruit + ' will cost ' + fivePrice + ' €')
// }

// fiveCost(fruit, price)

// -------------------------3 task ---------------------------------

// const myForm = document.getElementById("myForm");

// myForm.addEventListener('submit', (e)=> {
//     e.preventDefault();

//     const fruit = document.getElementsByName('fruit')[0] 
//     const quantity = document.getElementsByName('quantity')[0]
//     const price = document.getElementsByName('price')[0]

//     document.getElementById('atsakymas').innerText = `Vaisius: ${fruit.value}, Kiekis: ${quantity.value}, Kaina: ${price.value}`
// })


// -------------------------4 task ---------------------------------

const heroes = [
    {
        name: 'Batman',
        skill: 'Has a lot of money'
    },
    {
        name: 'Spider-man',
        skill: 'Launches webs'
    },
    {
        name: 'Iron-man',
        skill: 'Can fly'
    },
    {
        name: 'Super woman',
        skill: 'Has a magical whip'
    }
]

heroes.forEach((ele, index) => {
    const newDiv = document.createElement('div');
    newDiv.innerText = `${ele.name} - ${ele.skill}`
    if(index % 2 !== 0) {
        newDiv.style.backgroundColor = 'blue'
        newDiv.style.color = 'white'
    }
    
    document.body.appendChild(newDiv)

})